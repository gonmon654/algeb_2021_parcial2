﻿using UnityEngine;
using EjerciciosAlgebra;

public class Ejercicio1: MonoBehaviour
{
    [SerializeField] float angle;
    Vector3 vector1 = new Vector3(10, 0, 0);
    void Start()
    {
        VectorDebugger.AddVector(vector1, "Vector 1");
        VectorDebugger.EnableCoordinates();
        VectorDebugger.EnableEditorView();
    }
    void Update()
    {
        Vector3 newDirection =  QuaternionClass.Euler(0, angle * Time.deltaTime, 0) * VectorDebugger.GetVectorsPositions("Vector 1")[1];
        VectorDebugger.UpdatePosition("Vector 1", newDirection);
    }
}
