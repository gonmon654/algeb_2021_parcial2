﻿using System;
using UnityEngine.Internal;
using UnityEngine;

public struct QuaternionClass : IEquatable<QuaternionClass>
{
    #region Useful Stuff
    public Vector3 xyz //Allows easy access to the main 3 portions of the vector
    {
        set
        {
            x = value.x;
            y = value.y;
            z = value.z;
        }
        get
        {
            return new Vector3(x, y, z);
        }
    }
    //public float Length 
    //{
    //    get
    //    {
    //        return (float)System.Math.Sqrt(x * x + y * y + z * z + w * w);
    //    }
    //}
    //public float LengthSquared
    //{
    //    get
    //    {
    //        return x * x + y * y + z * z + w * w;
    //    }
    //}
    #endregion

    #region Static Properties
    public const float kEpsilon = 1E-06f;
    public static QuaternionClass identity
    {
        get
        {
            return new QuaternionClass(0f, 0f, 0f, 1f);
        }
    }
    #endregion

    #region Properties
    public Vector3 eulerAngles
    {
        get => ToEuler();
        set => this = Euler(value);      
    }
    public QuaternionClass normalized
    {
        get
        {
            return Normalize(this);
        }
    }
    public float this[int index]
    {
        get
        {
            switch (index)
            {
                case 0:
                    return this.x;
                case 1:
                    return this.y;
                case 2:
                    return this.z;
                case 3:
                    return this.w;
                default:
                    throw new IndexOutOfRangeException("Invalid Quaternion index: " + index + ", can use only 0,1,2,3");
            }
        }
        set
        {
            switch (index)
            {
                case 0:
                    this.x = value;
                    break;
                case 1:
                    this.y = value;
                    break;
                case 2:
                    this.z = value;
                    break;
                case 3:
                    this.w = value;
                    break;
                default:
                    throw new IndexOutOfRangeException("Invalid Quaternion index: " + index + ", can use only 0,1,2,3");
            }
        }
    }
    public float x;
    public float y;
    public float z;
    public float w;
    #endregion

    #region Constructors
    public QuaternionClass(float x, float y, float z, float w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
    public QuaternionClass(Vector3 v, float w)
    {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
        this.w = w;
    }
    #endregion

    #region Public Methods
    public void Set(float new_x, float new_y, float new_z, float new_w)
    {
        this.x = new_x;
        this.y = new_y;
        this.z = new_z;
        this.w = new_w;
    }
    public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection)
    {
        this = QuaternionClass.FromToRotation(fromDirection, toDirection);
    }
    public void SetLookRotation(Vector3 view, [DefaultValue("Vector3.up")] Vector3 up)
    {
        this = QuaternionClass.LookRotation(view, up);
    }
    public void SetLookRotation(Vector3 view)
    {
        Vector3 up = Vector3.up;
        this.SetLookRotation(view, up);
    }
    public void ToAngleAxis(out float angle, out Vector3 axis)
    {
        angle = 2 * Mathf.Acos(w) * Mathf.Rad2Deg;
        axis.x = x / Mathf.Sqrt(1 - w * w);
        axis.y = y / Mathf.Sqrt(1 - w * w);
        axis.z = z / Mathf.Sqrt(1 - w * w);
    }
    public override string ToString()
    {
        return string.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", this.x, this.y, this.z, this.w);
    }
    public void Normalize()
    {
        this = Normalize(this);
    }
    #endregion

    #region Static Methods
    public static float Angle(QuaternionClass a, QuaternionClass b)
    {
        float f = QuaternionClass.Dot(a, b);
        return Mathf.Acos(Mathf.Min(Mathf.Abs(f), 1f)) * 2f * Mathf.Rad2Deg;
    }
    public static QuaternionClass AngleAxis(float angle, Vector3 axis)
    {
        if (axis.sqrMagnitude == 0.0f)
            return identity;

        QuaternionClass result;
        var radians = angle * Mathf.Deg2Rad;
        radians *= 0.5f;

        axis.Normalize();
        axis = axis * (float)System.Math.Sin(radians);

        result.x = axis.x;
        result.y = axis.y;
        result.z = axis.z;
        result.w = (float)System.Math.Cos(radians);

        return Normalize(result);
    }
    public static float Dot(QuaternionClass a, QuaternionClass b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
    }
    public static QuaternionClass Euler(float x, float y, float z)
    {
        QuaternionClass q;
        float num1 = Mathf.Cos(y / 2);
        float num2 = Mathf.Cos(z / 2);
        float num3 = Mathf.Cos(x / 2);
        float num4 = Mathf.Sin(y / 2);
        float num5 = Mathf.Sin(z / 2);
        float num6 = Mathf.Sin(x / 2);

        q.w = (num1 * num2 * num3) - (num4 * num5 * num6);
        q.x = (num4 * num5 * num3) + (num1 * num2 * num6);
        q.y = (num4 * num2 * num3) + (num1 * num5 * num6);
        q.z = (num1 * num5 * num3) - (num4 * num2 * num6);

        return q;
    }
    public static QuaternionClass Euler(Vector3 euler) => Euler(euler.x, euler.y, euler.z);
    public static QuaternionClass EulerRotation(float x, float y, float z) => Euler(x, y, z);
    public static QuaternionClass EulerRotation(Vector3 euler) => Euler(euler);
    public void SetEulerRotation(float x, float y, float z) => Euler(x, y, z);
    public void SetEulerRotation(Vector3 euler) => this = Euler(euler);
    public Vector3 ToEuler()
    {
        float test = (x * y) + (w * z);
        Vector3 v;

        if (test > 0.4999f)
        {
            v.y = 2f * Mathf.Atan2(y, x);
            v.x = Mathf.PI / 2;
            v.z = 0;
            return v * Mathf.Rad2Deg;
        }
        if (test < -0.4999f)
        {
            v.y = -2f * Mathf.Atan2(y, x);
            v.x = -Mathf.PI / 2;
            v.z = 0;
            return v * Mathf.Rad2Deg;
        }
        v.x = Mathf.Asin(2f * x * z - w * y);
        v.y = Mathf.Atan2(2f * x * w + x * y * z, 1 - 2f * (z * z + w * w));
        v.z = Mathf.Atan2(2f * x * y + 2f * z * w, 1 - 2f * (y * y + z * z));
        return v * Mathf.Rad2Deg;
    }
    public static QuaternionClass EulerAngles(float x, float y, float z) => Euler(x, y, z);
    public static QuaternionClass EulerAngles(Vector3 euler) => Euler(euler);
    public void ToAxisAngle(out Vector3 axis, out float angle) => ToAngleAxis(out angle, out axis);
    public void SetEulerAngles(float x, float y, float z) => Euler(x,y,z);
    public void SetEulerAngles(Vector3 euler) => Euler(euler);
    public static Vector3 ToEulerAngles(QuaternionClass rotation) => ToEulerAngles(rotation);
    public Vector3 ToEulerAngles() => ToEuler();
    public static QuaternionClass FromToRotation(Vector3 fromDirection, Vector3 toDirection)
    {
        return RotateTowards(LookRotation(fromDirection), LookRotation(toDirection), float.MaxValue);
    }
    public static QuaternionClass Inverse(QuaternionClass rotation)
    {
        QuaternionClass q;

        q.x = -rotation.x;
        q.y = -rotation.y;
        q.z = -rotation.z;
        q.w = rotation.w;

        return q;
    }
    public static QuaternionClass Lerp(QuaternionClass a, QuaternionClass b, float t)
    {
        if (t > 1) t = 1;
        if (t < 0) t = 0;
        return LerpUnclamped(a, b, t);
    }
    public static QuaternionClass LerpUnclamped(QuaternionClass a, QuaternionClass b, float t)
    {
        QuaternionClass q = identity;

        float time = 1 - t;
        if (Dot(a, b) >= 0f)
        {
            q.x = (time * a.x) + (t * b.x);
            q.y = (time * a.y) + (t * b.y);
            q.z = (time * a.z) + (t * b.z);
            q.w = (time * a.w) + (t * b.w);
        }
        else
        {
            q.x = (time * a.x) - (t * b.x);
            q.y = (time * a.y) - (t * b.y);
            q.z = (time * a.z) - (t * b.z);
            q.w = (time * a.w) - (t * b.w);
        }
        q.Normalize();

        return q;
    }
    public static QuaternionClass LookRotation(Vector3 forward, [DefaultValue("Vector3.up")] Vector3 upwards)
    {
        return QuaternionClass.LookRotation(ref forward, ref upwards);
    }
    public static QuaternionClass LookRotation(Vector3 forward)
    {
        Vector3 up = Vector3.up;
        return QuaternionClass.LookRotation(ref forward, ref up);
    }
    private static QuaternionClass LookRotation(ref Vector3 forward, ref Vector3 up)
    {
        forward = Vector3.Normalize(forward);

        Vector3 right = Vector3.Normalize(Vector3.Cross(up, forward));

        up = Vector3.Cross(forward, right);

        var m00 = right.x;
        var m01 = right.y;
        var m02 = right.z;

        var m10 = up.x;
        var m11 = up.y;
        var m12 = up.z;

        var m20 = forward.x;
        var m21 = forward.y;
        var m22 = forward.z;

        float num8 = (m00 + m11) + m22;
        var quaternion = new QuaternionClass();

        if (num8 > 0f)
        {
            var num = (float)System.Math.Sqrt(num8 + 1f);
            quaternion.w = num * 0.5f;
            num = 0.5f / num;
            quaternion.x = (m12 - m21) * num;
            quaternion.y = (m20 - m02) * num;
            quaternion.z = (m01 - m10) * num;
            return quaternion;
        }
        if ((m00 >= m11) && (m00 >= m22))
        {
            var num7 = (float)System.Math.Sqrt(((1f + m00) - m11) - m22);
            var num4 = 0.5f / num7;
            quaternion.x = 0.5f * num7;
            quaternion.y = (m01 + m10) * num4;
            quaternion.z = (m02 + m20) * num4;
            quaternion.w = (m12 - m21) * num4;
            return quaternion;
        }
        if (m11 > m22)
        {
            var num6 = (float)System.Math.Sqrt(((1f + m11) - m00) - m22);
            var num3 = 0.5f / num6;
            quaternion.x = (m10 + m01) * num3;
            quaternion.y = 0.5f * num6;
            quaternion.z = (m21 + m12) * num3;
            quaternion.w = (m20 - m02) * num3;
            return quaternion;
        }
        var num5 = (float)System.Math.Sqrt(((1f + m22) - m00) - m11);
        var num2 = 0.5f / num5;
        quaternion.x = (m20 + m02) * num2;
        quaternion.y = (m21 + m12) * num2;
        quaternion.z = 0.5f * num5;
        quaternion.w = (m01 - m10) * num2;
        return quaternion;
    }
    public static QuaternionClass Normalize(QuaternionClass q)
    {
        float num = Mathf.Sqrt(Dot(q, q));
        return num < kEpsilon ? identity : new QuaternionClass(q.x / num, q.y / num, q.z / num, q.w / num);
    }
    public static QuaternionClass RotateTowards(QuaternionClass from, QuaternionClass to, float maxDegreesDelta)
    {
        float num = QuaternionClass.Angle(from, to);
        if (num == 0f)
        {
            return to;
        }
        return QuaternionClass.Slerp(ref from, ref to, maxDegreesDelta / num);
    }
    public static QuaternionClass Slerp(QuaternionClass a, QuaternionClass b, float t)
    {
        return QuaternionClass.Slerp(ref a, ref b, t);
    }
    private static QuaternionClass Slerp(ref QuaternionClass a, ref QuaternionClass b, float t)
    {
        if (t > 1) t = 1;
        if (t < 0) t = 0;
        return SlerpUnclamped(ref a, ref b, t);
    }
    private static QuaternionClass SlerpUnclamped(ref QuaternionClass a, ref QuaternionClass b, float t)
    {
        // quaternion to return
        QuaternionClass qm = new QuaternionClass();
        // Calculate angle between them.
        float cosHalfTheta = a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;
        // if a=b or a=-b then theta = 0 and we can return a
        if (Mathf.Abs(cosHalfTheta) >= 1)
        {
            qm.w = a.w; qm.x = a.x; qm.y = a.y; qm.z = a.z;
            return qm;
        }
        // Calculate temporary values.
        float halfTheta = Mathf.Acos(cosHalfTheta);
        float sinHalfTheta = Mathf.Sqrt(1 - cosHalfTheta * cosHalfTheta);
        // if theta = 180 degrees then result is not fully defined
        // we could rotate around any axis normal to a or b
        if (Mathf.Abs(sinHalfTheta) < 0.001)
        { // fabs is floating point absolute
            qm.w = (a.w / 2 + b.w / 2);
            qm.x = (a.x / 2 + b.x / 2);
            qm.y = (a.y / 2 + b.y / 2);
            qm.z = (a.z / 2 + b.z / 2);
            return qm;
        }
        float ratioA = Mathf.Sin((1 - t) * halfTheta) / sinHalfTheta;
        float ratioB = Mathf.Sin(t * halfTheta) / sinHalfTheta;
        //calculate Quaternion.
        qm.w = (a.w * ratioA + b.w * ratioB);
        qm.x = (a.x * ratioA + b.x * ratioB);
        qm.y = (a.y * ratioA + b.y * ratioB);
        qm.z = (a.z * ratioA + b.z * ratioB);
        return qm;
    }
    #endregion

    #region Operators
    public static QuaternionClass operator *(QuaternionClass lhs, QuaternionClass rhs)
    {
        QuaternionClass q;
        q.x = lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y;
        q.y = lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z;
        q.z = lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x;
        q.w = lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z;
        return q;
    }
    public static Vector3 operator *(QuaternionClass rotation, Vector3 point)
    {
        float num1 = rotation.x * 2f;
        float num2 = rotation.y * 2f;
        float num3 = rotation.z * 2f;
        float num4 = rotation.x * num1;
        float num5 = rotation.y * num2;
        float num6 = rotation.z * num3;
        float num7 = rotation.x * num2;
        float num8 = rotation.x * num3;
        float num9 = rotation.y * num3;
        float num10 = rotation.w * num1;
        float num11 = rotation.w * num2;
        float num12 = rotation.w * num3;
        Vector3 vector3;
        vector3.x = ((1f - (num5 + num6)) * point.x + (num7 - num12) * point.y + (num8 + num11) * point.z);
        vector3.y = ((num7 + num12) * point.x + (1f - (num4 + num6)) * point.y + (num9 - num10) * point.z);
        vector3.z = ((num8 - num11) * point.x + (num9 + num10) * point.y + (1f - (num4 + num5)) * point.z);
        return vector3;
    }
    private static bool IsEqualUsingDot(float dot) => (double)dot > 0.999998986721039;
    public static bool operator ==(QuaternionClass lhs, QuaternionClass rhs) => IsEqualUsingDot(Dot(lhs, rhs));
    public static bool operator !=(QuaternionClass lhs, QuaternionClass rhs) => !(lhs == rhs);
    public override int GetHashCode() => x.GetHashCode() ^ y.GetHashCode() << 2 ^ z.GetHashCode() >> 2 ^ w.GetHashCode() >> 1;
    public override bool Equals(object other) => other is QuaternionClass other1 && Equals(other1);
    public bool Equals(QuaternionClass other) => x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z) && w.Equals(other.w);
    #endregion
}