﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;

public class Ejercicio2 : MonoBehaviour
{
    [SerializeField] float angle;

    Vector3 vector1 = new Vector3(10, 0, 0);
    Vector3 vector2 = new Vector3(10, 10, 0);
    Vector3 vector3 = new Vector3(20, 10, 0);

    private List<Vector3> lista = new List<Vector3>();

    void Start()
    {
        lista.Add(vector1);
        lista.Add(vector2);
        lista.Add(vector3);

        VectorDebugger.EnableCoordinates();
        VectorDebugger.EnableEditorView();
        VectorDebugger.AddVectorsSecuence(lista, false, "Vectores");

        VectorDebugger.UpdateColor("Vectores", Color.blue);
    }

    void Update()
    {
        List<Vector3> updateLista = new List<Vector3>();
        for (short i = 0; i<lista.Count; i++)
        {
            Vector3 newDirection = QuaternionClass.Euler(0, angle * Time.deltaTime, 0) * VectorDebugger.GetVectorsPositions("Vectores")[i];
            updateLista.Add(newDirection);                   
        }
        VectorDebugger.UpdatePositionsSecuence("Vectores", updateLista);
    }
    
}
