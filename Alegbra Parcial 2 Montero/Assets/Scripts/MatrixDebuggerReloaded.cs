﻿using UnityEngine;
using UnityEditor;

public class MatrixDebuggerReloaded : EditorWindow
{
	public GameObject target;
	GUIStyle style = new GUIStyle();
	[MenuItem("Window/My4x4MatrixDebugger")]
	static void Init()
	{
		MatrixDebuggerReloaded window = (MatrixDebuggerReloaded)EditorWindow.GetWindow(typeof(MatrixDebuggerReloaded));
		window.Show();
	}

	private void OnGUI()
	{
		target = EditorGUILayout.ObjectField(target, typeof(GameObject), true) as GameObject;
		if (target != null)
		{
			target.transform.position = EditorGUILayout.Vector3Field("Position: ", target.transform.position);
			target.transform.rotation = Quaternion.Euler(EditorGUILayout.Vector3Field("Rotation: ", target.transform.rotation.eulerAngles));
			target.transform.localScale = EditorGUILayout.Vector3Field("Scale: ", target.transform.localScale);
			style.fontSize = 20;
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Translation matrix", style);
			DrawMatrix(My4x4Matrix.Translate(target.transform.position));
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Rotation matrix", style);
			DrawMatrix(My4x4Matrix.Rotate(target.transform.rotation));
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Scale matrix", style);
			DrawMatrix(My4x4Matrix.Scale(target.transform.localScale));
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("TRS matrix", style);
			DrawMatrix(My4x4Matrix.TRS(target.transform.position, target.transform.rotation, target.transform.localScale));
		}
	}

	private void DrawMatrix(My4x4Matrix matrix)
	{
		GUIStyle style = new GUIStyle();
		style.fontSize = 20;
		EditorGUILayout.LabelField("| " + matrix.r0c0.ToString("00.00") + " " + matrix.r0c1.ToString("00.00") + " " + matrix.r0c2.ToString("00.00") + " " + matrix.r0c3.ToString("00.00") + " |", style);
		EditorGUILayout.LabelField("| " + matrix.r1c0.ToString("00.00") + " " + matrix.r1c1.ToString("00.00") + " " + matrix.r1c2.ToString("00.00") + " " + matrix.r1c3.ToString("00.00") + " |", style);
		EditorGUILayout.LabelField("| " + matrix.r2c0.ToString("00.00") + " " + matrix.r2c1.ToString("00.00") + " " + matrix.r2c2.ToString("00.00") + " " + matrix.r2c3.ToString("00.00") + " |", style);
		EditorGUILayout.LabelField("| " + matrix.r3c0.ToString("00.00") + " " + matrix.r3c1.ToString("00.00") + " " + matrix.r3c2.ToString("00.00") + " " + matrix.r3c3.ToString("00.00") + " |", style);
	}
}
