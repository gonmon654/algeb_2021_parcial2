﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct My4x4Matrix: IEquatable<My4x4Matrix>
{
    #region Variables
    //Row 0
    public float r0c0; //Row 0 Column 0
    public float r0c1;
    public float r0c2;
    public float r0c3;

    //Row 1
    public float r1c0;
    public float r1c1;
    public float r1c2;
    public float r1c3;

    //Row 2
    public float r2c0;
    public float r2c1;
    public float r2c2;
    public float r2c3;

    //Row 3
    public float r3c0;
    public float r3c1;
    public float r3c2;
    public float r3c3;
    #endregion

    #region Static Properties
    //Identity Matrix (Diagonal 1s)
    static readonly My4x4Matrix identityMatrix = 
            new My4x4Matrix(
            new Vector4(1, 0, 0, 0),
            new Vector4(0, 1, 0, 0),
            new Vector4(0, 0, 1, 0),
            new Vector4(0, 0, 0, 1));
    public static My4x4Matrix identity
    {
        get
        {
            return identityMatrix;
        }
    }
    static readonly My4x4Matrix zeroMatrix = 
            new My4x4Matrix(
            new Vector4(0, 0, 0, 0),
            new Vector4(0, 0, 0, 0),
            new Vector4(0, 0, 0, 0),
            new Vector4(0, 0, 0, 0));

    public static My4x4Matrix zero { 
        get { 
            return zeroMatrix; 
        } 
    }
    public static My4x4Matrix Transpose(My4x4Matrix m)
    {
       My4x4Matrix transposed;

        transposed.r0c0 = m.r0c0;
        transposed.r0c1 = m.r1c0;
        transposed.r0c2 = m.r2c0;
        transposed.r0c3 = m.r3c0;

        transposed.r1c0 = m.r0c1;
        transposed.r1c1 = m.r1c1;
        transposed.r1c2 = m.r2c1;
        transposed.r1c3 = m.r3c1;

        transposed.r2c0 = m.r0c2;
        transposed.r2c1 = m.r1c2;
        transposed.r2c2 = m.r2c2;
        transposed.r2c3 = m.r3c2;

        transposed.r3c0 = m.r0c3;
        transposed.r3c1 = m.r1c3;
        transposed.r3c2 = m.r2c3;
        transposed.r3c3 = m.r3c3;

        return transposed;
    }
    #endregion

    #region Constructors
    public My4x4Matrix(Vector4 column0, Vector4 column1, Vector4 column2, Vector4 column3)
    {
        r0c0 = column0.x; r0c1 = column1.x; r0c2 = column2.x; r0c3 = column3.x;
        r1c0 = column0.y; r1c1 = column1.y; r1c2 = column2.y; r1c3 = column3.y;
        r2c0 = column0.z; r2c1 = column1.z; r2c2 = column2.z; r2c3 = column3.z;
        r3c0 = column0.w; r3c1 = column1.w; r3c2 = column2.w; r3c3 = column3.w;
    }
    public float this[int index]
    {
        get
        {
            switch (index)
            {
                case 0: 
                    return r0c0;
                case 1: 
                    return r1c0;
                case 2: 
                    return r2c0;
                case 3: 
                    return r3c0;
                case 4: 
                    return r0c1;
                case 5: 
                    return r1c1;
                case 6: 
                    return r2c1;
                case 7: 
                    return r3c1;
                case 8: 
                    return r0c2;
                case 9: 
                    return r1c2;
                case 10: 
                    return r2c2;
                case 11: 
                    return r3c2;
                case 12: 
                    return r0c3;
                case 13: 
                    return r1c3;
                case 14: 
                    return r2c3;
                case 15: 
                    return r3c3;
                default:
                    throw new IndexOutOfRangeException("Invalid matrix index!");
            }
        }

        set
        {
            switch (index)
            {
                case 0:
                    r0c0 = value;
                    break;
                case 1:
                    r1c0 = value;
                    break;
                case 2: 
                    r2c0 = value; 
                    break;
                case 3:
                    r3c0 = value; 
                    break;
                case 4:
                    r0c1 = value;
                    break;
                case 5:
                    r1c1 = value;
                    break;
                case 6:
                    r2c1 = value;
                    break;
                case 7: 
                    r3c1 = value;
                    break;
                case 8: 
                    r0c2 = value;
                    break;
                case 9: 
                    r1c2 = value;
                    break;
                case 10: 
                    r2c2 = value;
                    break;
                case 11:
                    r3c2 = value; 
                    break;
                case 12: 
                    r0c3 = value;
                    break;
                case 13:
                    r1c3 = value;
                    break;
                case 14: 
                    r2c3 = value;
                    break;
                case 15:
                    r3c3 = value; break;

                default:
                    throw new IndexOutOfRangeException("Invalid matrix index!");
            }
        }
    }
    #endregion

    #region Public Methods
    public Vector4 GetColumn(int index)
    {
        switch (index)
        {
            case 0: return new Vector4(r0c0, r1c0, r2c0, r3c0);
            case 1: return new Vector4(r0c1, r1c1, r2c1, r3c1);
            case 2: return new Vector4(r0c2, r1c2, r2c2, r3c2);
            case 3: return new Vector4(r0c3, r1c3, r2c3, r3c3);
            default:
                throw new IndexOutOfRangeException("Invalid column!");
        }
    }
    public Vector4 GetRow(int index)
    {
        switch (index)
        {
            case 0: return new Vector4(r0c0, r0c1, r0c2, r0c3);
            case 1: return new Vector4(r1c0, r1c1, r1c2, r1c3);
            case 2: return new Vector4(r2c0, r2c1, r2c2, r2c3);
            case 3: return new Vector4(r3c0, r3c1, r3c2, r3c3);
            default:
                throw new IndexOutOfRangeException("Invalid row!");
        }
    }
    public My4x4Matrix transpose => Transpose(this);
    public Vector3 lossyScale => new Vector3(r0c0, r1c1, r2c2);
    public My4x4Matrix inverse => Inverse(this);
    public Quaternion rotation
    {
        get
        {
            Quaternion q;

            q.w = Mathf.Sqrt(1 + r0c0 + r1c1 + r2c2) / 2;
            q.x = (r2c1 - r1c2) / (4 * q.w);
            q.y = (r0c2 - r2c0) / (4 * q.w);
            q.z = (r1c0 - r0c1) / (4 * q.w);

            return q;
        }
    }
    #endregion

    #region Static Methods
    public static My4x4Matrix Rotate(Quaternion quat)
    {
        float x = quat.x * 2.0F;
        float y = quat.y * 2.0F;
        float z = quat.z * 2.0F;

        float xx = quat.x * x;
        float yy = quat.y * y;
        float zz = quat.z * z;
        float xy = quat.x * y;
        float xz = quat.x * z;
        float yz = quat.y * z;
        float wx = quat.w * x;
        float wy = quat.w * y;
        float wz = quat.w * z;

        My4x4Matrix m;
        m.r0c0 = 1.0f - (yy + zz);
        m.r0c1 = xy + wz;          
        m.r0c2 = xz + wy;          
        m.r1c0 = xy - wz;          
        m.r1c1 = 1.0f - (xx + zz); 
        m.r1c2 = yz + wx;          
        m.r2c0 = xz - wy;          
        m.r2c1 = yz - wx;          
        m.r2c2 = 1.0f - (xx + yy);

        m.r0c3 = 0.0F;
        m.r1c3 = 0.0F;
        m.r2c3 = 0.0F;
        m.r3c0 = 0.0F;             
        m.r3c1 = 0.0F;             
        m.r3c2 = 0.0F;             
        m.r3c3 = 1.0F;
        return m;
    }
    public static My4x4Matrix Scale(Vector3 scal)
    {
        My4x4Matrix m;
        m.r0c0 = scal.x; m.r0c1 = 0F;     m.r0c2 = 0F;     m.r0c3 = 0F;
        m.r1c0 = 0F;     m.r1c1 = scal.y; m.r1c2 = 0F;     m.r1c3 = 0F;
        m.r2c0 = 0F;     m.r2c1 = 0F;     m.r2c2 = scal.z; m.r2c3 = 0F;
        m.r3c0 = 0F;     m.r3c1 = 0F;     m.r3c2 = 0F;     m.r3c3 = 1F;
        return m;
    }
    public static My4x4Matrix Translate(Vector3 pos)
    {
        My4x4Matrix m;
        m.r0c0 = 1F; m.r0c1 = 0F; m.r0c2 = 0F; m.r0c3 = pos.x;
        m.r1c0 = 0F; m.r1c1 = 1F; m.r1c2 = 0F; m.r1c3 = pos.y;
        m.r2c0 = 0F; m.r2c1 = 0F; m.r2c2 = 1F; m.r2c3 = pos.z;
        m.r3c0 = 0F; m.r3c1 = 0F; m.r3c2 = 0F; m.r3c3 = 1F;
        return m;
    }
    public static My4x4Matrix TRS(Vector3 pos, Quaternion quat, Vector3 scal)
    {
        My4x4Matrix translated = My4x4Matrix.Translate(pos);
        My4x4Matrix rotated = My4x4Matrix.Rotate(quat);
        My4x4Matrix scaled = My4x4Matrix.Scale(scal);
        My4x4Matrix m = translated * rotated * scaled;
        return m;
    }
    public static My4x4Matrix Inverse(My4x4Matrix matrix)
    {
        var s0 = matrix.r0c0 * matrix.r1c1 - matrix.r1c0 * matrix.r0c1;
        var s1 = matrix.r0c0 * matrix.r1c3 - matrix.r1c0 * matrix.r0c3;
        var s2 = matrix.r0c0 * matrix.r1c3 - matrix.r1c0 * matrix.r0c3;

        var s3 = matrix.r0c1 * matrix.r1c2 - matrix.r1c1 * matrix.r0c2;
        var s4 = matrix.r0c1 * matrix.r1c3 - matrix.r1c1 * matrix.r0c3;
        var s5 = matrix.r0c2 * matrix.r1c3 - matrix.r1c2 * matrix.r0c3;

        var c0 = matrix.r2c0 * matrix.r3c1 - matrix.r3c0 * matrix.r2c1;
        var c1 = matrix.r2c0 * matrix.r3c2 - matrix.r3c0 * matrix.r2c2;
        var c2 = matrix.r2c0 * matrix.r3c3 - matrix.r3c0 * matrix.r2c3;

        var c3 = matrix.r2c1 * matrix.r3c2 - matrix.r3c1 * matrix.r2c2;
        var c4 = matrix.r2c1 * matrix.r3c3 - matrix.r3c1 * matrix.r2c3;
        var c5 = matrix.r2c2 * matrix.r3c3 - matrix.r3c2 * matrix.r2c3;

        var det = (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);

        if (Mathf.Abs(det) < Mathf.Epsilon) 
        { 
            return My4x4Matrix.zero; 
        }
        else
        {
            My4x4Matrix m;

            m.r0c0 = (matrix.r1c1 * c5 - matrix.r1c2 * c4 + matrix.r1c3 * c3) * 1 / det;
            m.r0c1 = (-matrix.r0c1 * c5 + matrix.r0c2 * c4 - matrix.r0c3 * c3) * 1 / det;
            m.r0c2 = (matrix.r3c1 * s5 - matrix.r3c2 * s4 + matrix.r3c3 * s3) * 1 / det;
            m.r0c3 = (-matrix.r2c1 * s5 + matrix.r2c2 * s4 - matrix.r2c3 * s3) * 1 / det;


            m.r1c0 = (-matrix.r1c0 * c5 + matrix.r1c2 * c2 - matrix.r1c3 * c1) * 1 / det;
            m.r1c1 = (matrix.r0c0 * c5 - matrix.r0c2 * c2 + matrix.r0c3 * c1) * 1 / det;
            m.r1c2 = (-matrix.r3c0 * s5 + matrix.r3c2 * s2 - matrix.r3c3 * s1) * 1 / det;
            m.r1c3 = (matrix.r2c0 * s5 - matrix.r2c2 * s2 + matrix.r2c3 * s1) * 1 / det;

            m.r2c0 = (matrix.r1c0 * c4 - matrix.r1c1 * c2 + matrix.r1c3 * c0) * 1 / det;
            m.r2c1 = (-matrix.r0c0 * c4 + matrix.r0c1 * c2 - matrix.r0c3 * c0) * 1 / det;
            m.r2c2 = (matrix.r3c0 * s4 - matrix.r3c1 * s2 + matrix.r3c3 * s0) * 1 / det;
            m.r2c3 = (-matrix.r2c0 * s4 + matrix.r2c1 * s2 - matrix.r2c3 * s0) * 1 / det;

            m.r3c0 = (matrix.r1c0 * c3 + matrix.r1c1 * c1 - matrix.r1c2 * c0) * 1 / det;
            m.r3c1 = (-matrix.r0c0 * c3 - matrix.r0c1 * c1 + matrix.r0c2 * c0) * 1 / det;
            m.r3c2 = (matrix.r3c0 * s3 + matrix.r3c1 * s1 - matrix.r3c2 * s0) * 1 / det;
            m.r3c3 = (-matrix.r2c0 * s3 - matrix.r2c1 * s1 + matrix.r2c2 * s0) * 1 / det;

            return m;
        }
        /*       
        b[3, 0] = (-a[1, 0] * c3 + a[1, 1] * c1 - a[1, 2] * c0) * invdet;
        b[3, 1] = ( a[0, 0] * c3 - a[0, 1] * c1 + a[0, 2] * c0) * invdet;
        b[3, 2] = (-a[3, 0] * s3 + a[3, 1] * s1 - a[3, 2] * s0) * invdet;
        b[3, 3] = ( a[2, 0] * s3 - a[2, 1] * s1 + a[2, 2] * s0) * invdet;
        */
    }
    public void SetColumn(int index, Vector4 column)
    {
        switch (index)
        {
            case 0:
                r0c0 = column.x;
                r1c0 = column.y;
                r2c0 = column.z;
                r3c0 = column.w;
                break;
            case 1:
                r0c1 = column.x;
                r1c1 = column.y;
                r2c1 = column.z;
                r3c1 = column.w;
                break;
            case 2:
                r0c2 = column.x;
                r1c2 = column.y;
                r2c2 = column.z;
                r3c2 = column.w;
                break;
            case 3:
                r0c3 = column.x;
                r1c3 = column.y;
                r2c3 = column.z;
                r3c3 = column.w;
                break;
        }
    }
    public void SetRow(int index, Vector4 row)
    {
        switch (index)
        {
            case 0:
                r0c0 = row.x;
                r0c1 = row.y;
                r0c2 = row.z;
                r0c3 = row.w;
                break;
            case 1:
                r1c0 = row.x;
                r1c1 = row.y;
                r1c2 = row.z;
                r1c3 = row.w;
                break;
            case 2:
                r2c0 = row.x;
                r2c1 = row.y;
                r2c2 = row.z;
                r2c3 = row.w;
                break;
            case 3:
                r3c0 = row.x;
                r3c1 = row.y;
                r3c2 = row.z;
                r3c3 = row.w;
                break;
        }
    }
    public Vector3 MultiplyPoint(Vector3 point)
    {
        Vector3 res;
        float w;
        res.x = r0c0 * point.x + r0c1 * point.y + r0c2 * point.z + r0c3;
        res.y = r1c0 * point.x + r1c1 * point.y + r1c2 * point.z + r1c3;
        res.z = r2c0 * point.x + r2c1 * point.y + r2c2 * point.z + r2c3;
        w = r3c0 * point.x + r3c1 * point.y + r3c2 * point.z + r3c3;

        w = 1F / w;
        res.x *= w;
        res.y *= w;
        res.z *= w;
        return res;
    }
    public Vector3 MultiplyPoint3x4(Vector3 point)
    {
        Vector3 res;
        res.x = r0c0 * point.x + r0c1 * point.y + r0c2 * point.z + r0c3;
        res.y = r1c0 * point.x + r1c1 * point.y + r1c2 * point.z + r1c3;
        res.z = r2c0 * point.x + r2c1 * point.y + r2c2 * point.z + r2c3;
        return res;
    }
    public Vector3 MultiplyVector(Vector3 vector)
    {
        Vector3 res;
        res.x = r0c0 * vector.x + r0c1 * vector.y + r0c2 * vector.z;
        res.y = r1c0 * vector.x + r1c1 * vector.y + r1c2 * vector.z;
        res.z = r2c0 * vector.x + r2c1 * vector.y + r2c2 * vector.z;
        return res;
    }
    public Vector3 LossyScale()
    {
        Vector3 lossyScale = new Vector3(r0c0, r1c1, r2c2);
        return lossyScale;
    }
    #endregion

    #region Operators
    public static My4x4Matrix operator *(My4x4Matrix lhs, My4x4Matrix rhs)
    {
        My4x4Matrix res;
        
        res.r0c0 = lhs.r0c0 * rhs.r0c0 + lhs.r0c1 * rhs.r1c0 + lhs.r0c2 * rhs.r2c0 + lhs.r0c3 * rhs.r3c0;
        res.r0c1 = lhs.r0c0 * rhs.r0c1 + lhs.r0c1 * rhs.r1c1 + lhs.r0c2 * rhs.r2c1 + lhs.r0c3 * rhs.r3c1;
        res.r0c2 = lhs.r0c0 * rhs.r0c2 + lhs.r0c1 * rhs.r1c2 + lhs.r0c2 * rhs.r2c2 + lhs.r0c3 * rhs.r3c2;
        res.r0c3 = lhs.r0c0 * rhs.r0c3 + lhs.r0c1 * rhs.r1c3 + lhs.r0c2 * rhs.r2c3 + lhs.r0c3 * rhs.r3c3;

        res.r1c0 = lhs.r1c0 * rhs.r0c0 + lhs.r1c1 * rhs.r1c0 + lhs.r1c2 * rhs.r2c0 + lhs.r1c3 * rhs.r3c0;
        res.r1c1 = lhs.r1c0 * rhs.r0c1 + lhs.r1c1 * rhs.r1c1 + lhs.r1c2 * rhs.r2c1 + lhs.r1c3 * rhs.r3c1;
        res.r1c2 = lhs.r1c0 * rhs.r0c2 + lhs.r1c1 * rhs.r1c2 + lhs.r1c2 * rhs.r2c2 + lhs.r1c3 * rhs.r3c2;
        res.r1c3 = lhs.r1c0 * rhs.r0c3 + lhs.r1c1 * rhs.r1c3 + lhs.r1c2 * rhs.r2c3 + lhs.r1c3 * rhs.r3c3;

        res.r2c0 = lhs.r2c0 * rhs.r0c0 + lhs.r2c1 * rhs.r1c0 + lhs.r2c2 * rhs.r2c0 + lhs.r2c3 * rhs.r3c0;
        res.r2c1 = lhs.r2c0 * rhs.r0c1 + lhs.r2c1 * rhs.r1c1 + lhs.r2c2 * rhs.r2c1 + lhs.r2c3 * rhs.r3c1;
        res.r2c2 = lhs.r2c0 * rhs.r0c2 + lhs.r2c1 * rhs.r1c2 + lhs.r2c2 * rhs.r2c2 + lhs.r2c3 * rhs.r3c2;
        res.r2c3 = lhs.r2c0 * rhs.r0c3 + lhs.r2c1 * rhs.r1c3 + lhs.r2c2 * rhs.r2c3 + lhs.r2c3 * rhs.r3c3;

        res.r3c0 = lhs.r3c0 * rhs.r0c0 + lhs.r3c1 * rhs.r1c0 + lhs.r3c2 * rhs.r2c0 + lhs.r3c3 * rhs.r3c0;
        res.r3c1 = lhs.r3c0 * rhs.r0c1 + lhs.r3c1 * rhs.r1c1 + lhs.r3c2 * rhs.r2c1 + lhs.r3c3 * rhs.r3c1;
        res.r3c2 = lhs.r3c0 * rhs.r0c2 + lhs.r3c1 * rhs.r1c2 + lhs.r3c2 * rhs.r2c2 + lhs.r3c3 * rhs.r3c2;
        res.r3c3 = lhs.r3c0 * rhs.r0c3 + lhs.r3c1 * rhs.r1c3 + lhs.r3c2 * rhs.r2c3 + lhs.r3c3 * rhs.r3c3;
        return res;
    } //ignoremoslo 
    public static Vector4 operator *(My4x4Matrix lhs, Vector4 vector)
    {
        Vector4 res;
        res.x = lhs.r0c0 * vector.x + lhs.r0c1 * vector.y + lhs.r0c2 * vector.z + lhs.r0c3 * vector.w;
        res.y = lhs.r1c0 * vector.x + lhs.r1c1 * vector.y + lhs.r1c2 * vector.z + lhs.r1c3 * vector.w;
        res.z = lhs.r2c0 * vector.x + lhs.r2c1 * vector.y + lhs.r2c2 * vector.z + lhs.r2c3 * vector.w;
        res.w = lhs.r3c0 * vector.x + lhs.r3c1 * vector.y + lhs.r3c2 * vector.z + lhs.r3c3 * vector.w;
        return res;
    }
    public static bool operator ==(My4x4Matrix lhs, My4x4Matrix rhs)
    {
        return lhs.GetColumn(0) == rhs.GetColumn(0)
            && lhs.GetColumn(1) == rhs.GetColumn(1)
            && lhs.GetColumn(2) == rhs.GetColumn(2)
            && lhs.GetColumn(3) == rhs.GetColumn(3);
    }
    public static bool operator !=(My4x4Matrix lhs, My4x4Matrix rhs)
    {
        return !(lhs == rhs);
    }
    //public override bool Equals(object other) => other is My4x4Matrix other1 && this.Equals(other1);
    public bool Equals(My4x4Matrix other)
    {
        int num;
        if (this.GetColumn(0).Equals(other.GetColumn(0)))
        {
            Vector4 column = this.GetColumn(1);
            if (column.Equals(other.GetColumn(1)))
            {
                column = this.GetColumn(2);
                if (column.Equals(other.GetColumn(2)))
                {
                    column = this.GetColumn(3);
                    num = column.Equals(other.GetColumn(3)) ? 1 : 0;
                    goto label_5;
                }
            }
        }
        num = 0;
    label_5:
        return num != 0;
    }
    public override int GetHashCode()
    {
        Vector4 column = this.GetColumn(0);
        int hashCode = column.GetHashCode();
        column = this.GetColumn(1);
        int num1 = column.GetHashCode() << 2;
        int num2 = hashCode ^ num1;
        column = this.GetColumn(2);
        int num3 = column.GetHashCode() >> 2;
        int num4 = num2 ^ num3;
        column = this.GetColumn(3);
        int num5 = column.GetHashCode() >> 1;
        return num4 ^ num5;
    }
    public override bool Equals(object obj)
    {
        return obj is My4x4Matrix matrix &&
               r0c0 == matrix.r0c0 &&
               r0c1 == matrix.r0c1 &&
               r0c2 == matrix.r0c2 &&
               r0c3 == matrix.r0c3 &&
               r1c0 == matrix.r1c0 &&
               r1c1 == matrix.r1c1 &&
               r1c2 == matrix.r1c2 &&
               r1c3 == matrix.r1c3 &&
               r2c0 == matrix.r2c0 &&
               r2c1 == matrix.r2c1 &&
               r2c2 == matrix.r2c2 &&
               r2c3 == matrix.r2c3 &&
               r3c0 == matrix.r3c0 &&
               r3c1 == matrix.r3c1 &&
               r3c2 == matrix.r3c2 &&
               r3c3 == matrix.r3c3 &&
               EqualityComparer<My4x4Matrix>.Default.Equals(Transpose(matrix), matrix.transpose);
    }
    #endregion
}
