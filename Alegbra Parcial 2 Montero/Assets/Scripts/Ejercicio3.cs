﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;

public class Ejercicio3 : MonoBehaviour
{
    [SerializeField] float angle;

    Vector3 vector1 = new Vector3(10, 0, 0);
    Vector3 vector2 = new Vector3(10, 10, 0);
    Vector3 vector3 = new Vector3(20, 10, 0);
    Vector3 vector4 = new Vector3(20, 20, 0);

    private List<Vector3> lista = new List<Vector3>();

    void Start()
    {
        lista.Add(vector1);
        lista.Add(vector2);
        lista.Add(vector3);
        lista.Add(vector4);

        VectorDebugger.EnableCoordinates();
        VectorDebugger.EnableEditorView();
        VectorDebugger.AddVectorsSecuence(lista, false, "Vectores");
        VectorDebugger.UpdateColor("Vectores", Color.yellow);
    }
    void Update()
    {
        List<Vector3> updateLista = new List<Vector3>();
        for (short i = 0; i < lista.Count; i++)
        {
            if (i % 2 == 1)
            {
                if (i == 1)
                {
                    Vector3 newDirection = QuaternionClass.Euler(angle * Time.deltaTime, angle * Time.deltaTime, 0) * VectorDebugger.GetVectorsPositions("Vectores")[i];
                    updateLista.Add(newDirection);
                }
                else
                {
                    Vector3 newDirection = QuaternionClass.Euler(-angle * Time.deltaTime, -angle * Time.deltaTime, 0) * VectorDebugger.GetVectorsPositions("Vectores")[i];
                    updateLista.Add(newDirection);
                }
                
            }
            else
            {
                updateLista.Add(lista[i]);
            }           
        }
        VectorDebugger.UpdatePositionsSecuence("Vectores", updateLista);
    }
}
